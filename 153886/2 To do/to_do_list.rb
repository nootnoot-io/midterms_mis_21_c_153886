class ToDoList
	def initialize
		@task_list = []
		@atask_list = []
		@i = 0
	end
	def add_task(task_name)
		@task = task_name
		@task_list << @task
		puts "#{@task} has been added to your task list"
	end
	def view_pending_tasks
		puts "Pending Tasks:"
		i = 1
		@task_list.each do |task|
			puts "(#{i}) #{task}"
			i += 1
		end
	end
	def accomplish_task(int)
		puts "#{@task_list.slice(int)} has been marked Accomplished"
		@atask_list << @task_list.delete_at(int)
	end
	def view_accomplished_tasks
		puts "Accomplished Tasks:"
		i = 1
		@atask_list.each do |task|
			puts "(#{i}) #{task}"
			i += 1
		end
	end
end

todo = ToDoList.new



puts "================ \n"



todo.add_task("Clean room")

todo.add_task("Submit MIS21 HW")

todo.add_task("Study for MIS21 midterms")

todo.add_task("Study for Stat midterms")

todo.add_task("Submit advisement form")

todo.add_task("Consult for final project")



puts "================ \n"



todo.view_pending_tasks



puts "================ \n"

todo.accomplish_task(0)

todo.accomplish_task(2)

todo.accomplish_task(2)



puts "================ \n"



todo.view_accomplished_tasks

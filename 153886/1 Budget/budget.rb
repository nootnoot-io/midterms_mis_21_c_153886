require 'date'
class Budget
	attr_accessor :balance, :total_expense, :total_income

	def initialize(balance)
		@balance = balance
		@total_expense = Hash.new
		@total_income = Hash.new
		puts "Initial Balance: #{balance}"
	end
	def add_expense(item, amount, date)
		@balance -= amount
		@date = Date.strptime(date, '%Y-%m-%d')
		puts "#{item}: An amount of #{amount} was deducted from the balance on #{@date.strftime('%B %e, %Y')}."
		@total_expense[@date.strftime('%B %Y')] = @total_expense[@date.strftime('%B %Y')].to_i + amount
	end
	def add_income (item, amount, date)
		@balance += amount
		@date = Date.strptime(date, '%Y-%m-%d')
		puts "#{item}: An amount of #{amount} was deducted from the balance on #{@date.strftime('%B %e, %Y')}."
		@total_income[@date.strftime('%B %Y')] = @total_income[@date.strftime('%B %Y')].to_i + amount
	end

	def monthly_report(month, year)
		date = "#{month}-#{year}"
		report = Date.strptime(date, '%m-%Y')
		puts "MONTHLY REPORT: #{report.strftime('%B %Y')}"
		puts "Total Expenses: #{total_expense[report.strftime('%B %Y')]}"
		puts "Total Income: #{total_income[report.strftime('%B %Y')]}"
	end
	def current
		puts "Balace: #{@balance}"
	end
end